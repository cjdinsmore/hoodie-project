<?php
require_once 'vendor/autoload.php';

class hoodie_db {
    function __construct() {
        $config = parse_ini_file('config.ini');
        $db = new MysqliDb('127.0.0.1', 'webadmin', $config['mysql_webadmin_pass'], 'hoodie_db');
    }

    function insert_email($email) {
        $data = Array(
            'email' => $email
        );
        $db = MysqliDb::getInstance();
        $id = $db->insert('emails', $data);
        return $id;
    }
}

?>