#!/usr/bin/python

import requests
from lxml import html
import re
import json
import smtplib
import pymysql.cursors
import email

page = requests.get('https://store.taylorswift.com/snake-hooded-pullover.html')
tree = html.fromstring(page.content)

scripts = tree.xpath('//script/text()')

hoodiesRestocked = False

if scripts:
    attributeDict = None
    for script in scripts:
        m = re.search("var amPossible[\d*]+\s=\s(.*})\n;", script, flags=re.DOTALL)
        if m:
            attributeDict = json.loads(m.group(1))

    if 'data' in attributeDict:
        if len(attributeDict['data']['attributes']) != 0:
            hoodiesRestocked = True

if hoodiesRestocked:
    connection = pymysql.connect(host='localhost',
                                 user='webadmin',
                                 password='K@U3hxXKa@w5',
                                 db='hoodie_db',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    email_recipients = None
    try:
        with connection.cursor() as cursor:
            sql = "SELECT id, email, email_sent FROM emails"
            cursor.execute(sql)
            results = cursor.fetchall()
            email_recipients = results
    except Exception:
        print("Something went wrong with SQL...")
    finally:
        connection.close()

    email_successes = []

    if email_recipients:
        for email_recipient in email_recipients:
            message = """\
From: noreply@filipinofire.org
To: %s
Subject: Taylor Swift Hoodies Are Back!

The hoodie is back in stock! Hurry up and get it here: https://store.taylorswift.com/snake-hooded-pullover.html
            """ % (email_recipient['email'])
            if email_recipient['email_sent'] == 0:
                try:
                    smtpObj = smtplib.SMTP('localhost')
                    smtpObj.sendmail('noreply@filipinofire.org', email_recipient['email'], message)
                    email_successes.append({'email': email_recipient['email'], 'id': email_recipient['id']})
                except Exception:
                    print("Error: unable to send email")

    if email_successes:
        new_connection = pymysql.connect(host='localhost',
                                 user='webadmin',
                                 password='K@U3hxXKa@w5',
                                 db='hoodie_db',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
        for email_success in email_successes:
            try:
                with new_connection.cursor() as cursor:
                    sql = "UPDATE emails SET email_sent=1 WHERE id=" + str(email_success['id'])
                    cursor.execute(sql)
                new_connection.commit()
            except Exception as e:
                print('Issue updating MySQL db')
        new_connection.close()
