<?php
    require_once __DIR__ . '/mysql.php';
    $success = false;
    $message = "";
    if (isset($_POST['email']))
    {
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            $hoodie_db = new hoodie_db();
            $id = $hoodie_db->insert_email($_POST['email']);
            if ($id) {
                echo '<h4 style="color: green">Signup successful!</h4>';
            }
	    else {
	    	echo '<h4 style="color: red">Something went wrong. PM /u/TheFilipinoFire and let him know!</h4>';
   	    }
        } else {
            echo '<h4 style="color: red">Please input a valid email.</h4>';
        }
    }
?>

<html>
 <head>
  <title>Taylor Swift Hoodie Mailing List</title>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <style>
    html,
    body,
    .view {
        height: 100%;
    }

    .view {
        background: url("https://store.taylorswift.com/mm5/graphics/00000001/hoodedfinal.jpg")no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

    .email_disclaimer {
        display: block;
    }

    .form-group {
        background-color: lightgrey;
    }
  </style>
 </head>
 <body>
    <div class="view">
        <!--Intro content-->
        <div class="full-bg-img flex-center">
            <form class="email_form" action="index.php" method="POST">
                <div class="form-group col-md-4 col-md-offset-4">
                    <h3>Hoodie Mailing List</h3>
                    <p class="lead">
                        I wrote up a script that checks the merch page for <a href="https://store.taylorswift.com/snake-hooded-pullover.html">this hoodie</a> every 30 minutes to check if there are more in stock.
                        If you would like to be emailed when more come in stock, feel free to sign up!
                    </p>
                    <label for="email_input">Email address</label>
                    <input name="email" type="email" class="form-control" id="email_input" aria-describedby="email" placeholder="Enter email">
                    <small id="email_disclaimer" class="form-text text-muted">I'll never share your email with anyone else.</small>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <!--/Intro content-->
    </div>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
 </body>
</html>
